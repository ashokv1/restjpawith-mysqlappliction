package com.example.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.exceptions.DetailsNotFoundException;
import com.example.demo.model.SBICustomer;
import com.example.demo.services.SBICuctomerService;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("/SBI")
public class SBICustomerController {

	private static final Logger LOG = LoggerFactory.getLogger(SBICuctomerService.class);

	@Autowired
	RestTemplate restTemplate;

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public SBICustomerController(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	@Autowired
	SBICuctomerService sbicuctomerService;

	@GetMapping(value = "/getAllCustomers")
	public List<SBICustomer> getAllCustomers() throws JsonProcessingException {

		List<SBICustomer> list = sbicuctomerService.getCustomerDetails();
		return list;
	}

	@PostMapping(value = "/createCustomer", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Async("asynctask")
	@Transactional(rollbackFor = DetailsNotFoundException.class)
	public ResponseEntity<SBICustomer> createCustomer(@Valid @RequestBody SBICustomer sbic) {
		long start = System.currentTimeMillis();
		SBICustomer scc = null;
		try {
			String url = "http://dev-tech.clix-capital.com/InquiryAgentOriginal/doGet.service/comRequest";
			scc = sbicuctomerService.createCustomer(sbic);
			SBICustomer sbicustomer1 = restTemplate.getForObject(url, SBICustomer.class);
			CompletableFuture.completedFuture(sbicustomer1);
			LOG.info("Elapsed time: " + (System.currentTimeMillis() - start));

		} catch (DetailsNotFoundException e) {
			// TODO: handle exception
			LOG.error("Customer data is not available");
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(scc);// ResponseEntity.accepted()(CompletableFuture.completedFuture(scc));

	}

	@PutMapping(value = "/updateCustomer", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public SBICustomer updateCustomer(@RequestBody SBICustomer sbic) {

		return sbicuctomerService.updateCustomer(sbic);
	}

	@DeleteMapping(value = "/deleteCustomer/{customerid}")
	public String deleteCustomer(@PathVariable(value = "customerid") long customerid) {

		sbicuctomerService.deleteCustomer(customerid);

		return "Deleted successfully";
	}

	@Scheduled(fixedRate = 3000)
	public void fixedRate() {
		LOG.info("Fixed Rate Task :::" + new Date());
	}

	@Scheduled(fixedDelay = 3000)
	public void fixedDelay() {
		LOG.info("Fixed Delay Rate Task ::::" + new Date());
		try {
			TimeUnit.SECONDS.sleep(7);
		} catch (InterruptedException ex) {
			LOG.error("Ran into an error {}", ex);
			throw new IllegalStateException(ex);
		}
	}

	@Scheduled(fixedRate = 2000, initialDelay = 5000)
	public void initialDelay() {
		LOG.info("Fixed Rate Task with Initial Delay :: Execution Time - {}" + new Date());
	}

	@Scheduled(cron = "* * * * * ?")
	public void scheduleTaskWithCronExpression() {
		LOG.info("Cron Task :: Execution Time - {}" + new Date());
	}
}