package com.example.demo;

import java.util.concurrent.Executor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class SBcurdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SBcurdApplication.class, args);
	}

	@Bean("asynctask")
	public Executor tasExecutor() {
		ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
		threadPoolTaskExecutor.setCorePoolSize(3);
		threadPoolTaskExecutor.setMaxPoolSize(5);
		threadPoolTaskExecutor.setQueueCapacity(50);
		threadPoolTaskExecutor.setThreadNamePrefix("SBICustomerController");
		threadPoolTaskExecutor.initialize();
		return threadPoolTaskExecutor;

	}

}
